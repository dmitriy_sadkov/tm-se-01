package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.repository.TaskRepository;

public class TaskServise {
    private TaskRepository taskRepository = new TaskRepository();

    public void saveTask(String taskName) {
        if(taskName==null||taskName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
            taskRepository.saveTask(new Task(taskName));
            System.out.println("[OK]");
        }
    }

    public void removeTask(String taskName) {
        if(taskName==null||taskName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
            taskRepository.removeTask(taskName);
            System.out.println("[OK]");
        }
    }

    public void showTasks() {
        if(taskRepository.isEmpty()){
            System.out.println("[NO TASKS]");
        }else {
            int i =1;
            for (Task task: taskRepository.getTaskList()) {
                System.out.println(i+". "+task.getTaskName());
            }
        }
    }

    public void clearTasks() {
        taskRepository.clearTasks();
    }
}
