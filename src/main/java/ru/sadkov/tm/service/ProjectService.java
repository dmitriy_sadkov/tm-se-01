package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.repository.ProjectRepository;

public class ProjectService {
    private ProjectRepository projectRepository = new ProjectRepository();


    public void saveProject(String projectName) {
        if(projectName==null||projectName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
            projectRepository.saveProject(new Project(projectName));
            System.out.println("[OK]");
        }
    }

    public void removeProject(String projectName) {
        if(projectName==null||projectName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
            projectRepository.removeProject(projectName);
            System.out.println("[OK]");
        }
    }

    public void showProjects() {
        if(projectRepository.isEmpty()){
            System.out.println("[NO PROJECTS]");
        }else {
           // System.out.println("[PROJECT LIST]");
            int i =1;
            for (Project project: projectRepository.getProjectList()) {
                System.out.println(i+". "+project.getProjectName());
            }
        }
    }

    public void clearProjects() {
        projectRepository.clearProjects();
    }
}
