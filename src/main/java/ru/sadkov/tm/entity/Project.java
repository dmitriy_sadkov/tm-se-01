package ru.sadkov.tm.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitriy Sadkov
 */

public class Project {
        private String projectName;
        private List<Task> taskList;

    public Project(String projectName) {
        this.projectName = projectName;
        taskList = new ArrayList<Task>();
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }
}
