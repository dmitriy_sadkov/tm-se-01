package ru.sadkov.tm.entity;

public class Task {
    private String taskName;
    private Project project;

    public Task(String taskName) {
        this.taskName = taskName;
        this.project = new Project("Test");
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
