package ru.sadkov.tm.main;

import ru.sadkov.tm.service.ProjectService;
import ru.sadkov.tm.service.TaskServise;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ProjectService projectService = new ProjectService();
        TaskServise taskServise = new TaskServise();
        System.out.println("*** WELCOME TO TASK MANAGER");
        System.out.println("enter HELP for command list");
        System.out.println("enter EXIT for exit");
        String command = "";
        do{
            command = scanner.nextLine().toLowerCase();
            switch(command){
                case "project-create":{
                    System.out.println("[PROJECT CREATE]");
                    System.out.println("[ENTER NAME:]");
                    String projectName = scanner.nextLine();
                    projectService.saveProject(projectName);
                    break;
                }
                case "project-remove":{
                    System.out.println("[PROJECT REMOVE]");
                    System.out.println("[ENTER NAME:]");
                    String projectName = scanner.nextLine();
                    projectService.removeProject(projectName);
                    break;
                }
                case "project-list":{
                    System.out.println("[PROJECTS:]");
                    projectService.showProjects();
                    break;
                }
                case "project-clear":{
                    System.out.println("[PROJECTS CLEAR]");
                    projectService.clearProjects();
                    break;
                }
                case "task-create":{
                    System.out.println("[TASK CREATE]");
                    System.out.println("[ENTER NAME:]");
                    String taskName = scanner.nextLine();
                    taskServise.saveTask(taskName);
                    break;
                }
                case "task-remove":{
                    System.out.println("[TASK REMOVE]");
                    System.out.println("[ENTER NAME:]");
                    String taskName = scanner.nextLine();
                    taskServise.removeTask(taskName);
                    break;
                }
                case "task-list":{
                    System.out.println("[TASKS:]");
                    taskServise.showTasks();
                    break;
                }
                case "task-clear":{
                    System.out.println("[TASKS CLEAR]");
                   taskServise.clearTasks();
                   break;
                }
                case "help":{
                    System.out.println("help: Show all commands");
                    System.out.println("project-clear: Remove all project");
                    System.out.println("project-remove: Remove selected project");
                    System.out.println("project-create: Create new projects");
                    System.out.println("project-list: Shaw all project");
                    System.out.println("task-create: Create new task");
                    System.out.println("task-list: Show all tasks");
                    System.out.println("task-remove: Remove selected task");
                    System.out.println("task-clear: Remove all Tasks");
                    break;

                }
                default:{
                    System.out.println("[INCORRECT COMMAND]");
                }

            }
        }while (!command.equalsIgnoreCase("exit"));

    }
}
