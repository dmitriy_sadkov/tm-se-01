package ru.sadkov.tm.repository;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ProjectRepository {
    private List<Project> projectList = new ArrayList<Project>();

    public boolean isEmpty(){
        return projectList.isEmpty();
    }

    public List<Project> getProjectList(){
        return projectList;
    }


    public void saveProject(Project project) {
        projectList.add(project);
    }

    public void removeProject(String projectName) {
        Iterator<Project> iterator = projectList.iterator();
        while(iterator.hasNext()){
            Project nextProject = iterator.next();
            if(nextProject.getProjectName().equalsIgnoreCase(projectName)){
                iterator.remove();
            }
        }
    }

    public void clearProjects() {
        projectList.clear();
    }
}
