package ru.sadkov.tm.repository;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TaskRepository {
    private List<Task> taskList = new ArrayList<>();

    public boolean isEmpty(){
        return taskList.isEmpty();
    }

    public List<Task> getTaskList(){
        return taskList;
    }


    public void saveTask(Task task) {
        taskList.add(task);
    }

    public void removeTask(String taskName) {
        Iterator<Task> iterator = taskList.iterator();
        while(iterator.hasNext()){
            Task nextTask = iterator.next();
            if(nextTask.getTaskName().equalsIgnoreCase(taskName)){
                iterator.remove();
            }
        }
    }

    public void clearTasks() {
        taskList.clear();
    }
}
